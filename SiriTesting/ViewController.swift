//
//  ViewController.swift
//  SiriTesting
//
//  Created by Kyryl Nevedrov on 02/05/2018.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import Intents

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        INPreferences.requestSiriAuthorization { (status) in
            print(status)
        }
        INVocabulary.shared().setVocabularyStrings(["pull up", "sit down"], of: .workoutActivityName)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

